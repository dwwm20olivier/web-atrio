import axios from 'axios'
export default class Panier {

    constructor(element) {
        if (element === null) {
            return;
        }
        this.element = element
        this.value = element.querySelector('.qte').value
        this.header = document.querySelector('.sp_p');
        this.price = element.querySelector('input[type=hidden]');
        let down = element.querySelector('a.down')
        let up = element.querySelectorAll('a.up')
        let remove = element.querySelectorAll('a.remove')
        this.info();
        up.forEach(element => {
            
            element.addEventListener('click', (e) => {
                this.value++
                let el = e.target.parentNode
                console.log(e.target.parentNode)
                this.up(el.id)
    
            })
        });

        down.addEventListener('click', (e) => {
            this.value--
            let el = e.target.parentNode
            this.down(el.id)
        })
        remove.forEach(element => {
            element.addEventListener('click', (e) => {
                e.preventDefault()
                this.remove(e.currentTarget.getAttribute('href'))
            })
        });

    }

    up(id) {
        axios.get('/panier/add/' + id);
        this.element.querySelector('.qte').value = this.value
        this.header.innerHTML = this.value
        this.element.querySelector('.total').innerHTML = "Total " + (this.price.value * this.value) + "€ TTC"
        axios.get('/panier/info').then((resp)=>{
            let somme=0;
            for(let elem in resp.data){
                somme+=resp.data[elem].quantite*resp.data[elem].product.price 
                console.log(somme)
                document.querySelector('.btn_valider span').innerHTML =  somme + "€ <strong>TTC</strong>";
            }
        })

    }

    down(id) {
        axios.get('/panier/less/' + id);
        this.element.querySelector('.qte').value = this.value
        this.element.querySelector('.total').innerHTML = "Total " + (this.price.value * this.value) + "€ TTC";
        axios.get('/panier/info').then((resp)=>{
            let somme=0;
            for(let elem in resp.data){
                somme+=resp.data[elem].quantite*resp.data[elem].product.price 
                document.querySelector('.btn_valider span').innerHTML =  somme + "€ <strong>TTC</strong>";
            }
        })

        this.header.innerHTML = this.value

    }

    remove(url) {
        axios.get(url);
        this.element.querySelector('.qte').value = 0
        this.header.innerHTML = 0
        document.querySelector('.total').innerHTML = "Total 0 € TTC";
        document.location.href='/panier';
        axios.get('/panier/info').then((resp)=>{
            let somme=0;
            for(let elem in resp.data){
                somme+=resp.data[elem].quantite*resp.data[elem].product.price 
                console.log(somme)
                document.getElementById('total'+resp.data[elem].id).innerHTML = "Total 0 € TTC";
                document.querySelector('.btn_valider span').innerHTML =  somme + "€ <strong>TTC</strong>";
            }
        })


    }

    info(){
        axios.get('/panier/info').then((resp)=>{
            let somme=0;
            for(let elem in resp.data){
                somme+=resp.data[elem].quantite*resp.data[elem].product.price 
                console.log(somme)
                document.querySelector('.btn_valider span').innerHTML =  somme + "€ <strong>TTC</strong>";
            }
        })
    }
}