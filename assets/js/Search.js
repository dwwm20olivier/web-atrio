
export default class Search{

    constructor(element){
        if(element==null){
            return;
        }
        this.form = element.querySelector('.js-search-form');
        this.search = element.querySelector('.searchPopup');
        this.search.style.display = "none";
        let link = element.querySelector('input[name=q]');
        link.addEventListener('keyup',(e)=>{
            if(e.target.value.length>2){
                this.loadForm();

            }
        })
        
    }

    loadForm(){
        const data = new FormData(this.form);
        const url = new URL(window.location.href);
        const params = new URLSearchParams();
        data.forEach((value,key)=>{
            params.append(key,value);
        })
        this.loadAjax(url.pathname+'?'+params.toString());
    }

    async loadAjax(url){
        const response = await fetch(url,{
            headers:{
                'X-Requested-With':'XMLHttpRequest'
            }
        })
        const data = await response.json();
        this.search.style.display = "block";
        this.search.innerHTML=data.search;
        document.querySelector('.btn-popup').addEventListener('click',(e)=>{
            this.close();
        })

        //history.replaceState({},'',url);
    }

    close(){
        this.search.style.display = "none";

    }
}