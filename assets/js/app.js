/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.scss';

import Filter from './Filter';
import Search from './Search';
import Panier from './Panier';
import axios from 'axios'

require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');


// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
// import $ from 'jquery';

/**Animation de la page d'accueil intersection observer */
const options = {
    root:null,
    rootMargin:"10px",
    threshold:.3
}
document.querySelectorAll('.quantity').forEach((element)=>{

    new Panier(element)
})

const handle = (entries,observer)=>{
    entries.forEach(entrie=>{
        if(entrie.intersectionRatio>.3){
            entrie.target.classList.add('reveal-visible')
            observer.unobserve(entrie.target)
        }
    })
}
window.addEventListener('DOMContentLoaded',()=>{
    const observer = new IntersectionObserver(handle, options)
    const targets = document.querySelectorAll('[class*="reveal-"]')
    targets.forEach(target => {
        observer.observe(target)
    });
    /** Search class pour rechercher des articles */
    new Search(document.querySelector('.js-search-filter'));
})

/** appel de l'object filter  */
new Filter(document.querySelector('.js-filter'));



/**Création d'une animation pour la page show.html.twig
 * animation des images
 */
class Lightbox{

    static init(){
        let links = Array.from(document.querySelectorAll('.photo a'))
        let gallery = links.map(element=>element.getAttribute('href'))

        links.forEach(element => {
            element.addEventListener('click',(e)=>{
                e.preventDefault();
                new Lightbox(e.currentTarget.getAttribute('href'),gallery)
            })
        });
    }

    constructor(url,gallery){
        this.element = this.buildForm(url)
        this.url = url
        this.gallery = gallery
        let div = document.querySelector('.container_show')
        div.appendChild(this.element)
        this.loadImage(url)
    }

    loadImage(url){
        const image = new Image()
        this.url = ''
        const container = document.querySelector('.lightbox_container')
        container.innerHTML = ""
        image.onload = ()=>{
            container.appendChild(image)
            this.url = url
        }
        image.src = url
    }

    close(){
        this.element.parentElement.removeChild(this.element) 
    }

    prev(e){
        let pos = this.gallery.findIndex(image => image===this.url)
        if(pos===0){
            pos = this.gallery.length
        }
        this.loadImage(this.gallery[pos-1])
    }

    next(){
        let pos = this.gallery.findIndex(image => image === this.url)
        if(pos=== this.gallery.length - 1){
            pos = -1
        }
        this.loadImage(this.gallery[pos+1])
    }

    buildForm(url){
        const dom = document.createElement('div')
        dom.classList.add('gallery')
        dom.innerHTML = `
        <button class="close"><span class="material-icons">
        close
        </span></button>
        <button class="prev"><span class="material-icons">
        navigate_before
        </span></button>
        <div class="lightbox_container">
        <img src="${url}" />
        </div>
        <button class="next"><span class="material-icons">
        navigate_next
        </span></button>
        `
        dom.querySelector('.close').addEventListener('click',()=>{this.close()})
        dom.querySelector('.prev').addEventListener('click',this.prev.bind(this))
        dom.querySelector('.next').addEventListener('click',this.next.bind(this))
        return dom
    }
}
Lightbox.init()

/** Animation icône hamburger menu max-width:900px */
let button = document.querySelector('.hamburger');
let boolean = false;
if(button){

    button.addEventListener('click',()=>{
        if(boolean===false){
            document.body.classList.add('move');
            boolean=true
        }else{
            document.body.classList.remove('move');
            boolean=false
        }
    })
}


/**Affichage de la popup pour le panier dans la barre de menu */

let value = [];
axios.get('/panier/info').then(resp=>{
    value=resp;
    for(let elem in resp.data){
        let p = document.createElement('p');
        let img = new Image(50)
        img.src = resp.data[elem].product.image
        let pr = document.createElement('p')
        pr.style.color = "#8b3e78"
        
        p.innerHTML = resp.data[elem].product.model
        pr.innerHTML = resp.data[elem].product.price + " €"
        document.querySelector('.result_panier').appendChild(img)
        document.querySelector('.result_panier').appendChild(p)
        document.querySelector('.result_panier').appendChild(pr)
    }
})

document.querySelector('.header_panier').addEventListener('mouseover',()=>{
    document.querySelector('.popup_panier').style.display = "flex"
})
document.querySelector('.header_panier').addEventListener('mouseleave',()=>{
    document.querySelector('.popup_panier').style.display = "none"
})

if(document.querySelector('span.list')){
document.querySelector('span.list').addEventListener('click',function(){

    document.querySelector('.search').classList.toggle('move_search')
});
}