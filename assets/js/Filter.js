
export default class Filter{
    constructor(element){
        if(element==null){
            return
        }
        this.form = element.querySelector('.js-form');
        this.content = element.querySelector('.js-content');
        this.pagination = element.querySelector('.navigation');
        this.price = element.querySelector('.price');
        this.bindEvents()
    }
    /**
     * Ecoute les évenements
     */
    bindEvents(){
        let links = this.form.querySelectorAll('input')
        links.forEach(element => {
            element.addEventListener('change',()=>{
                this.loadForm()
            })
        });
        let select = document.querySelector('select')
        select.addEventListener('change',()=>{
            this.loadForm()
        })
    }
    /**
     * Récupère les valeurs du formulaire et construit une url avec ses valeurs
     */
    loadForm(){
        const data = new FormData(this.form)
        const url = new URL(window.location.href)
        const params = new URLSearchParams()
        data.forEach((value, key) => {
            params.append(key, value)
        });
        this.loadUrl(url.pathname + '?' + params.toString())
    }

    /**
     * requête ajax
     * @param {string} url 
     */
    async loadUrl(url){
        const response = await fetch(url,{headers:{
            'X-Requested-With':'XMLHttpRequest'
        }})

        const data = await response.json()
        this.content.innerHTML = data.content
        this.pagination.innerHTML = data.pagination
        this.price.innerHTML = data.price
        history.replaceState({},'',url)
    }
}