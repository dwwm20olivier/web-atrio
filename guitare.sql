-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: guitare
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `marque_id` int(11) DEFAULT NULL,
  `categorie_id` int(11) DEFAULT NULL,
  `main_id` int(11) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double NOT NULL,
  `promo` tinyint(1) NOT NULL,
  `caracteristques` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_promo` double DEFAULT NULL,
  `prix_promo` double DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BFDD31684827B9B2` (`marque_id`),
  KEY `IDX_BFDD3168BCF5E72D` (`categorie_id`),
  KEY `IDX_BFDD3168627EA78A` (`main_id`),
  CONSTRAINT `FK_BFDD31684827B9B2` FOREIGN KEY (`marque_id`) REFERENCES `marques` (`id`),
  CONSTRAINT `FK_BFDD3168627EA78A` FOREIGN KEY (`main_id`) REFERENCES `domaine` (`id`),
  CONSTRAINT `FK_BFDD3168BCF5E72D` FOREIGN KEY (`categorie_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,1,1,1,'American Original ‘50s Stratocaster (USA, MN) - inca silver','Toujours plus fidèle au modèle original des années 50, la guitare électrique solidbody FENDER American Original ‘50s Stratocaster (0110112824) va combler les amateurs de sensations et de sonorités vintage, qui ne possède pas le budget pour une Custom Shop Time Machine.\r\nUne attention particulière a été portée sur les micros \"Pure Vintage ‘59\" avec aimants alnico 5, véritables calques des micros d\'époque.\r\nLivrée en étui Fender style vintage.','strat-50s-american-original-usa.jpg','strat-50s-american-original-usa-1.jpg','strat-50s-american-original-usa-2.jpg',1868,0,'Ref : 93257\r\n- Couleur : gris\r\n- Custom shop : non\r\n- Droitier / gaucher : droitier\r\n- Forme guitare électrique : str (ou proche)\r\n- Nombre de cordes guitare électrique : 6 cordes\r\n- FENDER American Original ‘50s Stratocaster\r\n- SKU Fender 0110112824\r\n- Guitare électrique solid body\r\n- American Original Series\r\n- 2020\r\n- Made in USA\r\n- Corps Frêne\r\n- Manche vissé en Erable massif, profil \"Soft V\"\r\n- Touche en Erable, 21x frettes vintage-tall\r\n- Diapason 25.5\" - 648 mm\r\n- Radius 9.5” (241 mm)\r\n- Largeur manche 1e frette 1.650\" (42 mm)\r\n- 3x micros simple-bobinage Fender Pure Vintage ‘59 Single-Coil Strat (aimants alnico 5)\r\n- Volume général\r\n- Tonalité 1 (micro manche)\r\n- Tonalité 2 (micros chevalet/central)\r\n- Sélecteur micros 5-positions\r\n- Chevalet/vibrato Fender vintage style 6-Point synchronized tremolo\r\n- Mécaniques Pure Vintage Single Line Fender Deluxe\r\n- Finition nitrocellulose brillant\r\n- Livrée en étui Fender vintage style\r\n- Tirants de cordes recommandées (accordage standard) : 9.42, 9.46',NULL,1868,'American-Original-50s-Stratocaster-usa','2022-01-26 21:02:00'),(2,3,1,1,'AF75 BS Artcore - brown sunburst','L\'Ibanez AF75-BS est une Jazzbox milieu de gamme appartenant à la série Artcore qui propose une large sélection de guitares type demi ou plaine caisse (semi-hollow ou hollowbody) remarquablement fabriquées et pour un budget des plus raisonnables.\r\nL\'AF75 est destinée au plus puristes d\'entre nous, avec un tempérament traditionnel / vintage agrémenté par la qualité de production garantie par les méthodes de fabrication contemporaine éprouvées, et l\'expertise Ibanez.\r\nLes micros ACH possèdent un niveau de sortie modéré induisant chaleur et velouté mais restent néanmoins précis et réactifs grâce à leurs aimants céramique.','af55-tf-artcore-600-1.jpg','af55-tf-artcore-2014-tobacco-flat-600-2.jpg','af55-tf-artcore-2014-tobacco-flat-600-3.jpg',499,1,'Ref : 4788\r\n- Couleur : sunburst\r\n- Custom shop : non\r\n- Droitier / gaucher : droitier\r\n- Nom : Ibanez AF75\r\n- Catégorie : Guitare Electrique Hollow Body\r\n- Série : Artcore\r\n- Provenance : Asie\r\n- Table : Erable laminé\r\n- Dos & éclisses : Erable laminé\r\n- Profondeur de caisse : 70 mm\r\n- Manche : collé, Acajou\r\n- Touche : Palissandre\r\n- Frettes : 20 type Medium\r\n- Diapason : 628mm - 24.75\"\r\n- Largeur au sillet : 43 mm\r\n- Largeur à la dernière frette : 57 mm\r\n- Radius : 305mmR\r\n- Micros : config. HH, Ibanez ACH Ceramic\r\n- Contrôles : 1 volume général, 1 tonalité générale, sélecteur micros 3 postions\r\n- Chevalet : Ibanez ART1\r\n- Cordier : Ibanez VT60\r\n- Accastillage : chrome\r\n- Couleurs : Brown Sunburst\r\n- Accessoires : -\r\n- Référence Ibanez : 2510014023006',10,449.1,'ibanez-tf55-artcore','2022-01-26 23:06:00'),(3,4,1,1,'E-II M-II Neck Thru (Japan) - see thru black','La guitare électrique ESP E-II M-II Neck Thru (STBK) est une solid-body Hard-Metal produite dans les règles de l\'art par des luthiers japonais hautement expérimentés, qui mixe des caractéristiques empruntées aux standards américains référentiels : corps Aulne, table Erable flammé, manche conducteur, touche Ebène 24 frettes, micros actifs EMG 81, vibrato Floyd Rose Original (fabriqué en Allemagne).\r\nLivrée en étui ESP.','e-ii-m-ii-neck-thru-nt-fm-hh-emg-fr-eb-600-1.jpg','e-ii-m-ii-neck-thru-nt-fm-hh-emg-fr-eb-600-2.jpg','e-ii-m-ii-neck-thru-nt-fm-hh-emg-fr-eb-600-3.jpg',2249,0,'Ref : 90914\r\n- Couleur : noir\r\n- Custom shop : non\r\n- Droitier / gaucher : droitier\r\n- Forme guitare électrique : str (ou proche)\r\n- Nombre de cordes guitare électrique : 6 cordes\r\n- ESP E-II M-II Neck Thru\r\n- SKU GEV 2M2FM-STBLK\r\n- Guitare électrique solid body\r\n- Fabriquée au Japon\r\n- 2019\r\n- Corps en Aulne\r\n- Table Erable flammé\r\n- Manche traversant en Erable 3x parties, profil \"Thin U\"\r\n- Touche en Ebène, 24x frettes jumbo\r\n- Diapason 25.5\"\r\n- Largeur manche 1e frette 42 mm\r\n- Micros double-bobinage actifs EMG 81\r\n- Volume\r\n- Sélecteur micros 3 positions\r\n- Vibrato double blocage Floyd Rose Original\r\n- Mécaniques Gotoh Locking\r\n- Finition brillant\r\n- Livrée en étui ESP',NULL,2249,'esp-E-II M-II-Neck-japan-black','2022-01-26 23:17:00'),(4,5,3,1,'GS405B Floor Guitar Stand','Le stand pliable pour guitare et basse à poser au sol Hercules GS405B est robuste et très bien conçu, il est pourvu de mousses non abrasives pour le verni : l\'instrument ne repose jamais sur des parties métalliques mais sur un caoutchouc exclusif breveté (SFF).','mini-electric-guitar-stand-600-87352.jpg',NULL,NULL,42.45,0,'Ref : 4501\r\n- Type de stand : pose au sol\r\n- Nom : Hercules GS405B Guitar Floor Stand\r\n- Catégorie : stand guitare et basse à poser au sol\r\n- Autres caractéristiques : tête-embase-support pliables, caoutchouc spécifiquement mis au point (SFF) qui n\'attaque pas les vernis\r\n- Radius de la base : 270 mm (10.6\")\r\n- Poids : 1.88 kg\r\n- Capacité : 15 kg\r\n- Houuse de transport optionnelle : GSB001\r\n- Référence Hercules : GS405B',NULL,42.45,'hercule-stand-GS405B-floor','2022-01-26 23:30:00'),(5,6,1,1,'Excalibur Supra (MN) - natural matte','La guitare électrique solidbody VIGIER Excalibur Supra (VIG-VE6-CVS1-NAM-MC), si elle puise des influences ostensibles d\'un grand standard américain, fait honneur à la lutherie française avec un cahier des charges exigeant, un niveau de qualité de finitions, et des performances qui n\'ont rien à envier à certains modèles Custom Shop.\r\nSur une plateforme traditionnelle (corps en Aulne, manche en Erable, touche Erable ou Palissandre, diapason 25,6\"), c\'est un instrument polyvalent grâce à sa configuration de micros Dimarzio HSH (PAF Pro/FS1/Tone Zone) et à 24 frettes qui lui confère un registre étendu).\r\nLivrée en étui.','excalibur-supra-hsh-trem-mn-600.jpg','excalibur-supra-hsh-trem-mn-600-1.jpg',NULL,2775,0,'Ref : 83035\r\n- Couleur : naturel\r\n- Custom shop : non\r\n- Droitier / gaucher : droitier\r\n- Forme guitare électrique : str (ou proche)\r\n- Nombre de cordes guitare électrique : 6 cordes\r\n- VIGIER Excalibur Supra\r\n- SKU VIG-VE6-CVS1-NAM-MC\r\n- Guitare électrique solid body\r\n- Made in France\r\n- 2018\r\n- Corps 2 parties en Aulne vieillit naturellement\r\n- Manche vissé Erable vieillit au moins 3 ans, profil D\r\n- Touche Erable ou Palissandre), 24 frettes medium (+ frette zéro)\r\n- Diapason 650mm / 25.6\"\r\n- Radius 300mm / 11.81\"\r\n- Largeur de manche 1e frette 42 mm\r\n- Largeur de manche dernière frette 57.7 mm\r\n- Epaisseur manche 1e frette 19.5 mm\r\n- Epaisseur manche 12e frette 23 mm\r\n- Configuration de micros HSH Dimarzio (PAF Pro/FS1/Tone Zone)\r\n- Volume, tonalité, sélecteur 5 positions\r\n- Vibrato traditionnel sur pivots Vigier 2011 non locking tremolo\r\n- Mécaniques à blocage Vigier\r\n- Environ 3.5 kg\r\n- Livrée en étui',NULL,2775,'Excalibur-Supra-natural-matte','2022-01-27 07:25:00'),(6,2,1,1,'Les Paul Standard - heritage cherry sunburst','La guitare électrique solidbody GIBSON Les Paul Standard 2019 (LPS19HSCH1) combine les caractéristiques plébiscitées par les guitaristes qui cherchent une Les Paul à la fois traditionnelle et contemporaine : pour faire court, sa caisse est munie du nouveau système \"Ultra-Modern Weight Relief\" qui optimise le rapport poids/résonance, son profil de manche Asymetrical 1960 Slim Taper est un modèle de confort et ses micros Burstbuckers Pro Alnico V combinés à des options de splitage et d\'inversion de phase (via 4x potentiomètres push/pull) confèrent à la Les Paul Standard Standard 2019 une gamme de sonorités intarissable, du clair feutré et précis à la grosse saturation moderne.\r\nVendue en étui Gibson.\r\n\r\n* La caisse est munie de petits trous dont le positionnement a été informatiquement optimisé; ils permettent de diminuer le poids de la guitare sans pour autant dégrader son caractère initial.','custom-shop-les-paul-standard-1959-60th-1.jpg','custom-shop-les-paul-standard-1959-60th-2.jpg','custom-shop-les-paul-custom-2016-600-3.jpg',3599,0,'Ref : 82697\r\n- Couleur : sunburst\r\n- Custom shop : non\r\n- Droitier / gaucher : droitier\r\n- Forme guitare électrique : single cut\r\n- Nombre de cordes guitare électrique : 6 cordes\r\n- GIBSON Les Paul Standard 2019\r\n- SKU Gibson LPS19HSCH1\r\n- Guitare électrique solid body (caisse munie de chambres tonales)\r\n- Fabriquée aux USA\r\n- Série Nashville USA\r\n- Corps en Acajou massif avec chambres acoustiques (\"Ultra-modern Weight-relief\")\r\n- Table en Erable grade AAA\r\n- Manche collé en Acajou massif, profil Asymmetrical Slim Taper \'60\r\n- Touche Palissandre massif, 22-frettes Medium\r\n- Diapason 62.865 cm / 24.75\"\r\n- Rayon de touche / radius 12\"\r\n- Largeur manche 1e frette 4.3053cm / 1.695\"\r\n- Largeur manche fin de touche 5.7404cm / 2.260\"\r\n- Epaisseur manche 1e frette 0.800\"\r\n- Epaisseur manche 12e frette 0.875\"\r\n- Sillet en TekToid\r\n- Angle de tête 17°\r\n- Micros double-bobinage Gibson Burstbucker Pro Alnico V\r\n- Volume par micro avec push/pull (split bobines, simple/double bobinage)\r\n- Tonalité par micro avec push/pull (Pure Bypass/Phase Reverse)\r\n- Circuit Pure Bypass > le signal transite directement de la sortie micro vers la fiche jack pour une pureté optimale\r\n- DIP switching (http://www.gibson.com/News-Lifestyle/Gear-Tech/en-us/DIP-Split-and-Tap-2017-Gibson-Les-Paul-Pickups.aspx)\r\n- Volume 500K Non-Linear & Tone 500K Non-Linear\r\n- Sélecteur micros 3-positions\r\n- Chevalet Tune-o-matic & cordier Stop Bar\r\n- Mécaniques bain d\'huile Grover Keystone, ratio 14:01\r\n- Finition nitrocellulose, brillant\r\n- Livrée en étui Gibson\r\n- Tirants de cordes recommandés 10.46',8,3311.08,'Les Paul Standard - heritage cherry sunburst','2022-01-27 07:31:00'),(7,7,3,1,'2226 Burly Slinky 11-52 - jeu de 6 cordes','LE jeu de cordes incontournable 6 cordes guitare électrique ERNIE BALL 2226 Burly Slinky 11-52 Adopté par Eric Clapton, Jimmy Page, Slash, Angus Young et une légion de légendes .\r\n\r\nCes cordes sont fabriquées en respectant un cahier des charges très strict propre à Ernie Ball qui garantit une performance optimale. L\'âme hexagonale accueille un fil de nickel pour une durée de vie accrue.','slinky-nickel-wound-burly-11-52-hd-161667.jpg',NULL,NULL,7.65,0,'Ref : 86425\r\n- Conditionnement vente : jeu de 6 cordes\r\n- Tirant : custom\r\nTirant : 11-14-18p-30-42-52',NULL,7.65,'Ernie-ball-11-52','2022-01-27 07:48:00'),(8,1,1,1,'Player Stratocaster (MEX, MN) - polar white','La guitare électrique solidbody FENDER Player Stratocaster (0144502515) fait partie de l\'exhaustive gamme Player produite au Mexique et introduite en 2018, qui remplace la très populaire collection Standard mexicaine. De plus en plus performantes, les Player profitent de diverses améliorations telles que de nouveaux micros, des touches 22 frettes en Erable ou Pau Ferro, ou d\'un vibrato haute précision 2-Point Synchronized Tremolo.\r\nProposées à des tarifs attractifs et concurrentiels, les Player sont incontournables pour les musiciens qui cherchent un instrument fidèle à l\'esprit Fender original, avec un confort de jeu et un qualité de production modernes.','strat-player-mex-sss-1.jpg','strat-player-mex-sss-2.jpg','strat-player-mex-sss-3.jpg',649,0,'Ref : 80668\r\n- Couleur : blanc\r\n- Custom shop : non\r\n- Droitier / gaucher : droitier\r\n- Forme guitare électrique : str (ou proche)\r\n- Nombre de cordes guitare électrique : 6 cordes\r\n- FENDER Player Stratocaster\r\n- SKU Fender 014-4502-515\r\n- Guitare électrique solid body\r\n- 2018\r\n- Made in Mexico\r\n- Corps Aulne\r\n- Manche vissé en Erable massif, profil \"Modern C\"\r\n- Touche en Erable ou Pau Ferro, 22x frettes medium-jumbo\r\n- Diapason 25.5\" - 648 mm\r\n- Radius 9.5\" (241 mm)\r\n- Largeur manche 1e frette 1.650\" (42 mm)\r\n- 3x micros simple bobinage Fender Player Series Single-Coil\r\n- Volume général\r\n- Tonalité 1 (Micro manche/centre)\r\n- Tonalité 2 (Micro chevalet)\r\n- Sélecteur micros 5-positions\r\n- Chevalet/vibrato traditionnel Fender 2-Point Synchronized Tremolo with Bent Steel Saddles\r\n- Mécaniques Fender Standard\r\n- Finition caisse polyester brillant\r\n- Finition manche uréthane satin\r\n- Tirants de cordes recommandées (accordage standard) : 9.42, 9.46',NULL,649,'fender-strat-mex-polar-white','2022-01-27 08:14:00'),(9,8,2,1,'Tramontane T118ACE - brown sunburst','La guitare folk électro LAG Tramontane T118ACE (BRS) est une Auditorium qui se distingue par sa table massive en Cèdre rouge, d\'un dos/éclisses/manche en Khaya laminé (bois tropical appartenant à la famille des Acajou, avec un veinage proche de l\'Acajou du Honduras, et une sonorité avec des médiums présents, des basses et aigus feutrés) et une touche en Brownwood*.\r\nElle est pétrie de qualités, avec de superbes sonorités chaudes, riches et dynamiques, et un confort de jeu indéniable.','t100ace-tramontane-auditorium-cw-studiolag-600-1.jpg',NULL,NULL,509,0,'Ref : 92682\r\n- Couleur guitare acoustique : sunburst\r\n- Droitier / gaucher : droitier\r\n- Forme guitare acoustique : 00 / 000 / om / parlor\r\n- Nombre cordes guitare acoustique : 6 cordes\r\n- LAG Tramontane T118ACE\r\n- SKU GLA T118ACE-BRS\r\n- Guitare folk acoustique (cordes bronze/acier)\r\n- Auditorium pan coupé\r\n- 2020\r\n- Table Cèdre rouge massif\r\n- Dos & éclisses Khaya laminé\r\n- Manche collé (queue d\'aronde) en Khaya\r\n- Touche Brownwood, 20 frettes\r\n- Diapason 25.5\" - 650 mm\r\n- Largeur manche 1e frette 43 mm\r\n- Chevalet Brownwood\r\n- Pré-ampli Astro-LÂG\r\n- Mécaniques Lag bain d\'huile, ratio 18:1\r\n- Tige de régale du manche double sens\r\n- Finition manche satin et caisse brillant',NULL,509,'lag-Tramontane-T118ACE-brown-sunburst','2022-01-27 08:24:00'),(10,9,3,1,'Electric GB-DGF David Gilmour 10-48 - jeu de 6 cordes','Les cordes pour guitare électrique GHS David Gilmour Signature Blue Set GB-DGF sont en acier plaqué nickel. La collaboration entre David Gilmour et la firme GHS remonte à 1979 lorsqu\'il commence à utiliser les cordes Boomers sur le projet Pink Floyd \"The Wall\".','guit-elec-6c-david-gilmour-signature-blue-set-gbdgf-010-048-600-c38.jpg',NULL,NULL,7.6,0,'Ref : 24953\r\n- Conditionnement vente : jeu de 6 cordes\r\n- Tirant : regular _ medium\r\n- Nom : GHS David Gilmour Signature Blue Set GB-DGF\r\n- Catégories : accessoires, cordes\r\n- Conception : acier plaqué nickel\r\n- Tirants : .010 .012.016.DY28.DY38.DY48',NULL,7.6,'ghs-gilmour-10-48','2022-01-27 08:31:00'),(11,10,3,1,'ECG25 - jeu de 6 cordes','Les cordes D\'Addario Chrome sont filées avec de l\'acier poli, afin d\'obtenir une surface particulièrement douce.\r\nElle délivrent un son légèrement atténué, mais riche en harmoniques.\r\nCertainement la référence à filé plat la plus populaire !','guit-elec-6c-chromes-jazz-012-052-ecg25-600-78804.jpg',NULL,NULL,27.5,1,'Ref : 1877\r\n- Conditionnement vente : jeu de 6 cordes\r\n- Tirant : light\r\nCordes pour guitare électrique filet plat\r\nTirants 012-016-024-032-042-052',8,25.3,'d\'addario-chromes-12-52','2022-01-27 08:38:00'),(12,12,1,1,'Warrior JS32T - natural oil','Dotée d\'un design radical mais non moins ergonomique, ainsi que d\'un profil de manche \"Speed Neck\" très facile d\'accès, la guitare électrique solid body JACKSON Warrior JS32T (2910126557) plaira à tous les débutants Heavy-Metal : caisse Nato, manche vissé Erable, touche Amaranthe 24 frettes, micros double bobinage Jackson High-Output Humbucking (aimants céramique), et chevalet fixe.','warrior-js32t-hh-ht-ama-600-1.jpg','warrior-js32t-hh-ht-ama-600-2.jpg',NULL,360,1,'Ref : 90306\r\n- Couleur : naturel\r\n- Custom shop : non\r\n- Droitier / gaucher : droitier\r\n- Forme guitare électrique : heavy extrême\r\n- Nombre de cordes guitare électrique : 6 cordes\r\n- JACKSON Warrior JS32T\r\n- SKU Jackson 2910126557\r\n- Guitare électrique solid body\r\n- JS Series\r\n- 2019\r\n- Corps Nato\r\n- Manche vissé Erable, profil \"JS\"\r\n- Touche Amaranthe, 24x frettes Jumbo\r\n- Diapason 25.5\" - 64.8 cm (type Fender)\r\n- Largeur manche 1e frette 1.6875\" (42.86 mm)\r\n- Radius hybride 12\"-16\" (304.8 mm à 406.4 mm)\r\n- Config. micros HH Jackson High-Output Humbucking (aimants céramique)\r\n- Volume, Tone\r\n- Sélecteur micros 3-positions\r\n- Chevalet fixe Jackson® TOM-Style Adjustable String-Through-Body (cordes traversantes)\r\n- Mécaniques Jackson bain d\'huile\r\n- Finition caisse satinée\r\n- Finition manche satinée\r\n- Tirants de cordes recommandés en accord standard : 009.042, 009.046, 010.046',10,324,'Warrior-JS32T-natural-oil','2022-01-27 08:58:00'),(13,1,2,1,'Paramount PM-1 Standard All Mahogany Avec Etui - natural open pore','La strong>FENDER Paramount PM-1 Standard NE All-Mahogany est une Dreadnought acoustique milieu de gamme constituée d\'Acajou massif, très réactive et chaleureuse.\r\nLivrée en étui Fender.','pm-1-standard-all-mahogany-case-paramount-dreadnought-tout-acajou-2016-600-1.jpg','pm-1-standard-all-mahogany-case-paramount-dreadnought-tout-acajou-2016-600-2.jpg','pm-1-standard-all-mahogany-case-paramount-dreadnought-tout-acajou-2016-600-3.jpg',609,1,'Ref : 69803\r\n- Couleur guitare acoustique : naturel\r\n- Droitier / gaucher : droitier\r\n- Forme guitare acoustique : dreadnought\r\n- Nombre cordes guitare acoustique : 6 cordes\r\n- FENDER PM-1 Standard NE All-Mahogany\r\n- Guitare acoustique folk (cordes acier/bronze)\r\n- Style Dreadnought\r\n- SKU Fender 096-0297-221\r\n- Paramount Series\r\n- Année d\'introduction 2016\r\n- Table Acajou massif\r\n- Dos & éclisses Acajou massif\r\n- Barrages quartersawn scalloped X\r\n- Manche en Acajou, profil C\r\n- Touche Palissandre\r\n- 20x frettes style vintage\r\n- Diapason 25.3\" (643 mm)\r\n- Radius 15.75\" (400 mm)\r\n- Largeur de manche 1e frette 1.69” (43 mm)\r\n- Chevalet Palissandre\r\n- Sillets Graph Tech NuBon\r\n- Mécaniques Fender style vintage\r\n- Coloris Natural Open Pore\r\n- Verni satin\r\n- Livrée en étui Fender Deluxe Black Hardshell (Black Interior)\r\n- Tirants de cordes recommandés Extra-Light, Light',12,535.92,'Paramount-PM-1-Standard-All-Mahogany-Etui-natural-open-pore','2022-01-27 09:08:00'),(14,3,2,1,'AW54 OPN Artwood-open-pore-natural','La série Artwood est présentée par Ibanez comme un mélange de tradition et de modernité notamment dans les nouvelles techniques et procédés de fabrication.\r\nEt pour cette guitare acoustique Ibanez AW54-OPN la tradition c\'est cette magnifique table en acajou massif, bois également utilisé pour le dos, les éclisses et le manche. La touche est elle en palissandre tout comme le chevalet.','aw54-opn-artwood-dreadnought-tout-acajou-600-1.jpg','aw54-opn-artwood-dreadnought-tout-acajou-600-2.jpg',NULL,269,1,'Ref : 59809\r\n- Couleur guitare acoustique : naturel\r\n- Droitier / gaucher : droitier\r\n- Forme guitare acoustique : dreadnought\r\n- Nombre cordes guitare acoustique : 6 cordes\r\n- Modèle: Ibanez AWCE-OPN\r\n- Référence SKU: 2510012025082\r\n- Catégorie: guitare acoustique\r\n- format de la caisse : Dreadnought\r\n- table : bois d\'acajou massif\r\n- dos et éclisses : acajou laminé\r\n- manche : acajou, assemblage queue d\'aronde\r\n- touche : palissandre (bois de rose)\r\n- diapason : 651 mm\r\n- frettes : 20\r\n- sillet de tête : 43 mm\r\n- mécaniques : de type fermé à bain d\'huile, chromées\r\n- chevalet : palissandre avec sillet compensé',10,242.1,'AW54-OPN-Artwood-open-pore-natural','2022-01-27 09:19:00'),(15,1,3,1,'Glass Slide FGS1 Std Medium','Bottleneck en verre FENDER FGS1 Standard Medium de 69 mm de longueur et de 2 mm d\'épaisseur pour un jeu à la guitare offrant un son riche et chaud.','fgs1-glass-slide-hd-2-135058.jpg',NULL,NULL,5,0,'Ref : 30325\r\n- Type de materiaux : verre\r\nGlass Slide\r\nStandard Medium\r\nLongeur 69 mm\r\nEpaisseur 2 mm\r\nDiamètre intérieur 18 mm',NULL,5,'Glass-Slide-FGS1-Std-Medium','2022-01-27 09:22:00');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Guitare-électrique','electrique.jpg',NULL,'2022-01-26 20:42:00'),(2,'Guitare-acoustique','folk.jpg',NULL,'2022-01-26 20:43:00'),(3,'accessoire','accessoire.jpg',NULL,'2022-01-26 20:43:00');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('App\\Migrations\\Version20220125212601','2022-01-25 21:26:53',331),('App\\Migrations\\Version20220126181512','2022-01-26 18:15:30',2114),('App\\Migrations\\Version20220126194126','2022-01-26 19:41:43',50);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domaine`
--

DROP TABLE IF EXISTS `domaine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domaine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domaine`
--

LOCK TABLES `domaine` WRITE;
/*!40000 ALTER TABLE `domaine` DISABLE KEYS */;
INSERT INTO `domaine` VALUES (1,'Guitare'),(2,'Batterie'),(3,'Amplificateur'),(4,'Basse');
/*!40000 ALTER TABLE `domaine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marques`
--

DROP TABLE IF EXISTS `marques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marques`
--

LOCK TABLES `marques` WRITE;
/*!40000 ALTER TABLE `marques` DISABLE KEYS */;
INSERT INTO `marques` VALUES (1,'Fender','fender.png','2022-01-26 20:07:00'),(2,'Gibson','gibson.jpg','2022-01-26 20:08:00'),(3,'Ibanez','ibanez.jpg','2022-01-26 20:09:00'),(4,'ESP','esp.png','2022-01-26 23:13:00'),(5,'Hercules','hercules.jpg','2022-01-26 23:26:00'),(6,'Vigier','vigier.jpg','2022-01-27 07:28:00'),(7,'Ernie Ball','ernie-ball.jpg','2022-01-27 07:49:00'),(8,'Lag','lag.png','2022-01-27 08:21:00'),(9,'Ghs','ghs.png','2022-01-27 08:28:00'),(10,'D\'addario','d-addario.jpg','2022-01-27 08:33:00'),(11,'Takamine','takamine.jpg','2022-01-27 08:39:00'),(12,'Jackson','jackson.jpg','2022-01-27 08:53:00');
/*!40000 ALTER TABLE `marques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guitare_id` int(11) DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_4B3656606844CA0E` (`guitare_id`),
  CONSTRAINT `FK_4B3656606844CA0E` FOREIGN KEY (`guitare_id`) REFERENCES `articles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock`
--

LOCK TABLES `stock` WRITE;
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` VALUES (1,6,1),(2,3,2),(3,1,3),(4,5,4),(5,12,5),(6,2,6),(7,13,7),(8,14,8),(9,15,9),(10,NULL,10),(11,10,11),(12,9,12),(13,8,13),(14,7,14),(15,4,15),(16,11,60);
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'olivier@gmail.com','[]','$argon2id$v=19$m=65536,t=4,p=1$5Ra2/Atc77i6/IRE+VCMmA$jVnDmYGv1LGYUEyLZJXi7x8pKe9uQSyZED36Ywy9BNE');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-27 10:11:34
