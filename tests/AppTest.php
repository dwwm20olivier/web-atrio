<?php

namespace App\Test;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Repository\UserRepository;

class AppTest extends KernelTestCase{

    public function testAssert()
    {
        self::bootKernel();
        $count = self::$container->get(UserRepository::class)->count([]);
        $this->assertEquals(1,$count);
    }

}