<?php

namespace App\Tests\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase{
    public function testHome()
    {
        $client = static::createClient();
        $crawler = $client->request('GET','/');
        $this->assertResponseIsSuccessful();
    }

    public function testCountLi()
    {
        $client = static::createClient();
        $crawler = $client->request('GET','/');
        $this->assertCount(5, $crawler->filter('header li'));

    }
}