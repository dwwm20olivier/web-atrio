<?php

namespace App\Repository;

use App\Search\Search;
use App\Entity\Articles;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Knp\Component\Pager\Paginator;

/**
 * @method GuitareElectrique|null find($id, $lockMode = null, $lockVersion = null)
 * @method GuitareElectrique|null findOneBy(array $criteria, array $orderBy = null)
 * @method GuitareElectrique[]    findAll()
 * @method GuitareElectrique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticlesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Articles::class);
        $this->paginator = $paginator;
    }

    /**
     * @param null|App\Search\Search filtre de recherche
     * @param null|string slug sélctionne si on affiche les guitare par categorie
     * @return array retourne un tableau des guitares plus où moins filtrer
     */
    public function findArticle(?Search $search, ?string $slug, ?int $main)
    {
        $query = $this->createQueryBuilder('a')
            ->select('a', 'm', 'c', 's', 'd')
            ->join('a.marque', 'm')
            ->join('a.categorie', 'c')
            ->join('a.stock', 's')
            ->join('a.main', 'd')
            ->andWhere('a.main = :param')
            ->setParameter(':param', $main)
            ->orderBy('a.id', 'DESC');
        if (!empty($search->q())) {
            $query = $query->andWhere('m.name LIKE :q')
                ->setParameter(':q', "%{$search->q()}%");
        }
        if (!empty($search->marque())) {
            $query = $query->andWhere('m.name IN (:name)')
                            ->setParameter(':name', $search->marque());
        }
        if (!empty($search->min())) {
            $query = $query->andWhere('a.prixPromo >= (:min)')
                            ->setParameter(':min', $search->min);
        }
        if (!empty($search->max())) {
            $query = $query->andWhere('a.prixPromo <= (:max)')
                            ->setParameter(':max', $search->max());
        }
        if (!empty($search->order())) {
            $query = $query->orderBy("a.price", $search->order());
        }
        if (!empty($slug)) {
            $query = $query->andWhere("c.slug = :slug")
                            ->setParameter(':slug', $slug);
        }
        $query->getQuery();
        return $this->paginator->paginate($query, $search->page(), 6);
    }

    /**
     * @var null|string slug permet de retourner les articles filtrés par categorie
     * @var null|int main permet de filtrer les articles filtrés par domain guitare||batterie||ampli||basse
     * @return array retourne un tableau du nombre d'article par marque et le nom des marques
     */
    public function marque(?string $slug, int $main): array
    {
        $query = $this->createQueryBuilder('g')
            ->select('count(g.id) as nbre', 'm.name')
            ->join('g.marque', 'm')
            ->join('g.categorie', 'c')
            ->where('g.main=:main')
            ->setParameter(':main', $main)
            ->groupBy('m.name');
        if (!empty($slug)) {
            $query = $query
            ->andWhere('c.slug = :slug')
            ->setParameter(':slug', $slug);
        }
        return $query->getQuery()->getScalarResult();
    }

    /**
     * @var null|string $slug filtre par catégorie
     * @var null|Search permet de filtrer les articles par marque
     * @var int $main permet de filtrer les articles filtrés par domain guitare||batterie||ampli||basse
     * @return array retourne le prix min et max pour chaques marques
     */
    public function minMax(?Search $search, ?string $slug, int $main): array
    {
        $query = $this->createQueryBuilder('g')
            ->select('MIN(g.price) as min', 'MAX(g.price) as max', 'c.slug')
            ->join('g.marque', 'm')
            ->join('g.categorie', 'c');
            //->where('g.main = :main')
            //->setParameter(':main',$main);
        if (!empty($slug)) {
            $query = $query->andWhere('c.slug = :slug')
            ->setParameter(':slug', $slug);
        }
        if (!empty($search->marque())) {
            $query = $query->andWhere('m.name IN (:name)')
                            ->setParameter(':name', $search->marque());
        }

        return $query->getQuery()->getScalarResult();
    }

    /**
     * @param int
     * @return les categories pour les guitares
     */
    public function findCategorie(int $domain): array
    {
        return $this->createQueryBuilder('g')
                ->select('c.name', 'c.image', 'c.slug', 'g')
                ->join('g.main', 'd')
                ->join('g.categorie', 'c')
                ->where('g.main = :domain')
                ->setParameter(':domain', $domain)
                ->groupBy('c.name')
                ->orderBy('c.id', 'ASC')
                ->getQuery()
                ->getResult();
    }

    public function findSearch(Search $search)
    {
        $query = $this->createQueryBuilder('a')
                      ->select('a', 'm')
                      ->join('a.marque', 'm');
        if (!empty($search->q())) {
            $query = $query->where('m.name LIKE :q')
            ->setParameter(':q', "{$search->q()}%");
        }
        return $query->getQuery()->getResult();
    }
}
