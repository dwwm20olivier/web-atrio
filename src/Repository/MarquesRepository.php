<?php

namespace App\Repository;

use App\Entity\Marques;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Marques|null find($id, $lockMode = null, $lockVersion = null)
 * @method Marques|null findOneBy(array $criteria, array $orderBy = null)
 * @method Marques[]    findAll()
 * @method Marques[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarquesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Marques::class);
    }


    public function findMarque(int $main, ?string $slug): array
    {
        $query = $this->createQueryBuilder('m')
                        ->select('m', 'g', 'c')
                        ->join('m.guitareElectriques', 'g')
                        ->join('g.categorie', 'c')
                        ->where('g.main = :main')
                        ->setParameter(':main', $main);
        if (!empty($slug)) {
            $query->andWhere('c.slug = :slug')
                  ->setParameter(':slug', $slug);
        }
        return $query->getQuery()->getResult();
    }
}
