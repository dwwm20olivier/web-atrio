<?php

namespace App\Search;

use Symfony\Component\HttpFoundation\Request;

class Search
{
    public function marque(): ?array
    {
        $request = Request::createFromGlobals();
        $marque = $request->get('marque');
        return $marque;
    }

    public function q(): ?string
    {
        $request = Request::createFromGlobals();
        $q = $request->get('q');
        return $q;
    }
    public function min(): ?int
    {
        $request = Request::createFromGlobals();
        $min = $request->get('prixmin');
        return intval($min);
    }
    public function max(): ?int
    {
        $request = Request::createFromGlobals();
        $max = $request->get('prixmax');
        return intval($max);
    }
    public function order(): ?string
    {
        $request = Request::createFromGlobals();
        $order = $request->get('order');
        return $order;
    }
    public function page(): ?int
    {
        $request = Request::createFromGlobals();
        $page = $request->get('page', 1);
        return $page;
    }
}
