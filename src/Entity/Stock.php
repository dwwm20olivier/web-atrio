<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 */
class Stock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Articles", inversedBy="stock", cascade={"persist", "remove"})
     */
    private $guitare;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getGuitare(): ?Articles
    {
        return $this->guitare;
    }

    public function setGuitare(?Articles $guitare): self
    {
        $this->guitare = $guitare;

        return $this;
    }

    public function __toString(): string
    {
        return $this->quantite;
    }
}
