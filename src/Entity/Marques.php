<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarquesRepository")
 * @Vich\Uploadable
 *
 */
class Marques
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("articles")
     *
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Articles", mappedBy="marque")
     */
    private $guitareElectriques;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $images;


    /**
     * @Vich\UploadableField(mapping="marque", fileNameProperty="images")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->guitareElectriques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|GuitareElectrique[]
     */
    public function getGuitareElectriques(): Collection
    {
        return $this->guitareElectriques;
    }

    public function addGuitareElectrique(Articles $guitareElectrique): self
    {
        if (!$this->guitareElectriques->contains($guitareElectrique)) {
            $this->guitareElectriques[] = $guitareElectrique;
            $guitareElectrique->setMarque($this);
        }

        return $this;
    }

    public function removeGuitareElectrique(Articles $guitareElectrique): self
    {
        if ($this->guitareElectriques->contains($guitareElectrique)) {
            $this->guitareElectriques->removeElement($guitareElectrique);
            // set the owning side to null (unless already changed)
            if ($guitareElectrique->getMarque() === $this) {
                $guitareElectrique->setMarque(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getImages(): ?string
    {
        return $this->images;
    }

    public function setImages(string $images): self
    {
        $this->images = $images;

        return $this;
    }
    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;
        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($imageFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
