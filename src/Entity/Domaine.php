<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DomaineRepository")
 */
class Domaine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Articles", mappedBy="main")
     */
    private $guitareElectriques;

    public function __construct()
    {
        $this->guitareElectriques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|GuitareElectrique[]
     */
    public function getGuitareElectriques(): Collection
    {
        return $this->guitareElectriques;
    }

    public function addGuitareElectrique(Articles $guitareElectrique): self
    {
        if (!$this->guitareElectriques->contains($guitareElectrique)) {
            $this->guitareElectriques[] = $guitareElectrique;
            $guitareElectrique->setMain($this);
        }

        return $this;
    }

    public function removeGuitareElectrique(Articles $guitareElectrique): self
    {
        if ($this->guitareElectriques->contains($guitareElectrique)) {
            $this->guitareElectriques->removeElement($guitareElectrique);
            // set the owning side to null (unless already changed)
            if ($guitareElectrique->getMain() === $this) {
                $guitareElectrique->setMain(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->name;
    }
}
