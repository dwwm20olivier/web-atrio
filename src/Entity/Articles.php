<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticlesRepository")
 * @Vich\Uploadable
 */
class Articles
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("articles")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("articles")
     *
     */
    private $model;

    /**
     * @ORM\Column(type="text")
     *
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("articles")
     *
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="product_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $image2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $image3;

    /**
     * @ORM\Column(type="float")
     * @Groups("articles")
     *
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     */
    private $promo;

    /**
     * @ORM\Column(type="text")
     */
    private $caracteristques;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Marques", inversedBy="guitareElectriques")
     * @Groups("articles")
     *
     */
    private $marque;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $codePromo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $prixPromo;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Stock", mappedBy="guitare", cascade={"persist", "remove"})
     */
    private $stock;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="guitareElectriques")
     */
    private $categorie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Domaine", inversedBy="guitareElectriques")
     */
    private $main;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }


    public function setImageFile(File $imageFile = null)
    {
        $this->imageFile = $imageFile;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($imageFile) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }


    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
    public function getImage2(): ?string
    {
        return $this->image2;
    }

    public function setImage2(?string $image2): self
    {
        $this->image2 = $image2;

        return $this;
    }

    public function getImage3(): ?string
    {
        return $this->image3;
    }

    public function setImage3(?string $image3): self
    {
        $this->image3 = $image3;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPromo(): ?bool
    {
        return $this->promo;
    }

    public function setPromo(bool $promo): self
    {
        $this->promo = $promo;

        return $this;
    }

    public function getCaracteristques(): ?string
    {
        return $this->caracteristques;
    }

    public function setCaracteristques(string $caracteristques): self
    {
        $this->caracteristques = $caracteristques;

        return $this;
    }

    public function getMarque(): ?Marques
    {
        return $this->marque;
    }

    public function setMarque(?Marques $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getCodePromo(): ?float
    {
        return $this->codePromo;
    }

    public function setCodePromo(?float $codePromo): self
    {
        $this->codePromo = $codePromo;

        return $this;
    }

    public function getPrixPromo(): ?float
    {
        return ($this->price - ($this->price * ($this->codePromo / 100)));
    }

    public function setPrixPromo($prixPromo): self
    {
        $this->prixPromo = ($this->price - ($this->price * ($this->codePromo / 100)));

        return $this;
    }

    public function getStock(): ?Stock
    {
        return $this->stock;
    }

    public function setStock(?Stock $stock): self
    {
        $this->stock = $stock;

        // set (or unset) the owning side of the relation if necessary
        $newGuitare = null === $stock ? null : $this;
        if ($stock->getGuitare() !== $newGuitare) {
            $stock->setGuitare($newGuitare);
        }

        return $this;
    }

    public function __toString()
    {
        return $this->model;
    }

    public function getCategorie(): ?Category
    {
        return $this->categorie;
    }

    public function setCategorie(?Category $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getMain(): ?Domaine
    {
        return $this->main;
    }

    public function setMain(?Domaine $main): self
    {
        $this->main = $main;

        return $this;
    }
}
