<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Repository\ArticlesRepository;
use App\Repository\BassRepository;
use App\Search\Search;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BasseController extends AbstractController
{
    /**
     * @return une vue avec l'ensemble des articles
     * @Route("/basse", name="basse")
     */
    public function index(ArticlesRepository $repository, Request $request): Response
    {
        $search = new Search();
        $slug = "basse-electrique";
        $categorie = $repository->findCategorie(3);
        $items = $repository->findArticle($search, null, 3);
        $searchs = $repository->findSearch($search);
        $count = $repository->marque($slug, 3);
        $result = $repository->minMax($search, null, 3);
        $min = $result[0]['min'];
        $max = $result[0]['max'];
        if ($request->isXmlHttpRequest()) {   //Requête en ajax
            return new JsonResponse([
                'content' => $this->renderView('basse/product.html.twig', ['items' => $items]),
                'pagination' => $this->renderView('guitare/pagination.html.twig', compact('items')),
                'price' => $this->renderView('includes/price.html.twig', ['min' => $min,'max' => $max]),
                'search' => $this->renderView('includes/index.html.twig', ['searchs' => $searchs])
            ]);
        }
        return $this->render('basse/index.html.twig', [
           'items' => $items,
           'count' => $count,
           'categories' => $categorie,
           'min' => $min,
           'max' => $max
        ]);
    }

    /**
     * @return la page show.html.twig
     * @Route("/basse/{slug<[a-z-A-Z-0-9-]+>}", name="showBasse")
     */
    public function show(Articles $item): Response
    {
        return $this->render('basse/show.html.twig', compact('item'));
    }

    /**
     * @Route("/show/{slug}", name="showBasseByCategorie")
     */
    public function showByCategorie($slug, ArticlesRepository $repository, Request $request): Response
    {
        $search = new Search();
        $categorie = $repository->findCategorie(3);
        $items = $repository->findArticle($search, $slug, 3);
        $searchs = $repository->findSearch($search);
        $count = $repository->marque($slug, 3);
        $result = $repository->minMax($search, $slug, 3);
        $min = $result[0]['min'];
        $max = $result[0]['max'];
        if ($request->isXmlHttpRequest()) {   //requête en ajax
            return new JsonResponse([
                'content' => $this->renderView('basse/product.html.twig', ['items' => $items]),
                'pagination' => $this->renderView('guitare/pagination.html.twig', compact('items')),
                'price' => $this->renderView('includes/price.html.twig', ['min' => $min,'max' => $max]),
                'search' => $this->renderView('includes/index.html.twig', ['searchs' => $searchs])

                ]);
        }
        return $this->render('basse/index.html.twig', [
            'items' => $items,
            'count' => $count,
            'categories' => $categorie,
            'min' => $min,
            'max' => $max
         ]);
    }
}
