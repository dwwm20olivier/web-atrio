<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Search\Search;
use App\Repository\ArticlesRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\MarquesRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GuitareController extends AbstractController
{
    /**
     * @Route("/guitare", name="guitare")
     */
    public function index(ArticlesRepository $repository, Request $request, MarquesRepository $marquerepository): Response
    {
        $marques = $marquerepository->findMarque(1, null);
        $search = new Search();
        $categories = $repository->findCategorie(1);
        $items = $repository->findArticle($search, null, 1);    /** retourne les éléments de la table guitare_electrique*/
        $searchs = $repository->findSearch($search);
        $result = $repository->minMax($search, null, 1);        /** Récupère le prix min et le prix max par categorie */
        $min =  $result[0]['min'];        /** prix min par marque et par categorie */
        $max = $result[0]['max'];        /** prix max par marque et par catégorie */
        $count = $repository->marque(null, 1);
        /** requête ajax correspondant au filtre */
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->renderView('guitare/guitare.html.twig', compact('items')),
                'pagination' => $this->renderView('guitare/pagination.html.twig', compact('items')),
                'price' => $this->renderView('includes/price.html.twig', ['min' => $min,'max' => $max]),
                'search' => $this->renderView('includes/index.html.twig', ['searchs' => $searchs])
            ]);
        }
        return $this->render('guitare/index.html.twig', [
            'items' => $items,
            'categories' => $categories,
            'count' => $count,
            'min' => $min,
            'max' => $max,
            'marques' => $marques
        ]);
    }


    /**
     * @return une vue dun seul article
     * @Route("/guitare-show/{slug}",name="show")
     */
    public function show(Articles $item): Response
    {

        return $this->render('includes/show.html.twig', compact('item'));
    }



    /**
     * retourne une page avec la liste des articles filtrés par catégorie
     * @Route("/guitare/{slug}",name="showGuitareByCategory")
     */
    public function showByCategory($slug, ArticlesRepository $repository, Request $request, MarquesRepository $marquerepository): Response
    {
        $marques = $marquerepository->findMarque(1, $slug);
        $search = new Search();
        $categories = $repository->findCategorie(1);
        $count = $repository->marque($slug, 1);        /* retourne le nombre d'articles classées par marques */
        $result = $repository->minMax($search, $slug, 1);
        $min =  $result[0]['min']; /** prix minimum */
        $max = $result[0]['max'];   /** prix maximum  */
        $items = $repository->findArticle($search, $slug, 1);
        $searchs = $repository->findSearch($search);
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->renderView('guitare/guitare.html.twig', compact('items')),
                'pagination' => $this->renderView('guitare/pagination.html.twig', compact('items')),
                'price' => $this->renderView('includes/price.html.twig', ['min' => $min,'max' => $max]),
                'search' => $this->renderView('includes/index.html.twig', ['searchs' => $searchs])
            ]);
        }
        return $this->render('guitare/index.html.twig', [
            'items' => $items,
            'categories' => $categories,
            'min' => $min,
            'max' => $max,
            'categories' => $categories,
            'count' => $count,
            'marques' => $marques
            ]);
    }
}
