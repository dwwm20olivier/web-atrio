<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Entity\Category;
use App\Repository\ArticlesRepository;
use App\Search\Search;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class AmpliController extends AbstractController
{
    /**
     * @Route("/ampli", name="ampli")
     */
    public function index(ArticlesRepository $repository, Request $request)
    {
        $search = new Search();
        $items = $repository->findArticle($search, null, 4);
        $count = $repository->marque(null, 4);
        $categories = $repository->findCategorie(4);
        $result = $repository->minMax($search, null, 4);
        $min = $result[0]['min'];
        $max = $result[0]['max'];

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->renderView('ampli/product.html.twig', compact('items')),
                'pagination' => $this->renderView('guitare/pagination.html.twig', compact('items')),
                'price' => $this->renderView('includes/price.html.twig', ['min' => $min,'max' => $max]),


            ]);
        }
        return $this->render('ampli/index.html.twig', [
            'items' => $items,
            'count' => $count,
            'categories' => $categories,
            'min' => $min,
            'max' => $max
        ]);
    }

    /**
     * @Route("/ampli/{id}", name="ampli_show")
     */
    public function show(Articles $item): Response
    {
        return $this->render('ampli/show.html.twig', compact('item'));
    }

     /**
      * @Route("/ampli/{slug}", name="ampliCategorie")
      */
    public function showByCategorie(ArticlesRepository $repository, $slug, Request $request): Response
    {
        $search = new Search();
        $items = $repository->findArticle($search, $slug, 4);
        $count = $repository->marque($slug, 4);
        $categories = $repository->findCategorie(4);
        $result = $repository->minMax($search, $slug, 4);
        $min = $result[0]['min'];
        $max = $result[0]['max'];

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->renderView('ampli/product.html.twig', compact('items')),
                'pagination' => $this->renderView('guitare/pagination.html.twig', compact('items')),
                'price' => $this->renderView('includes/price.html.twig', ['min' => $min,'max' => $max]),


            ]);
        }
        return $this->render('ampli/index.html.twig', [
           'items' => $items,
           'count' => $count,
           'categories' => $categories,
           'min' => $min,
           'max' => $max
        ]);
    }
}
