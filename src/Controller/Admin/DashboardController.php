<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Domaine;
use App\Entity\Marques;
use App\Entity\Stock;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $routeBuilder = $this->get(AdminUrlGenerator::class);

        return $this->redirect($routeBuilder->setController(ArticlesCrudController::class)->generateUrl());

        //return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('MusicStore');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Articles', 'fa fa-home');
        yield MenuItem::linkToCrud('Category', 'fas fa-list', Category::class);
        yield MenuItem::linkToCrud('Marques', 'fas fa-list', Marques::class);
        yield MenuItem::linkToCrud('Domaine', 'fas fa-list', Domaine::class);
        yield MenuItem::linkToCrud('Stocks', 'fas fa-list', Stock::class);
    }
}
