<?php

namespace App\Controller\Admin;

use App\Entity\Articles;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ArticlesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Articles::class;
    }





    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('model'),
            TextareaField::new('description'),

            MoneyField::new('price')->setCurrency('EUR')->setCustomOption('storedAsCents', false),
            BooleanField::new('promo'),
            TextareaField::new('caracteristques'),
            AssociationField::new('marque'),
            IntegerField::new('codePromo'),
            IntegerField::new('prixPromo'),
            AssociationField::new('stock'),
            AssociationField::new('categorie'),
            AssociationField::new('main'),
            TextField::new('slug'),
            ImageField::new('image')->setBasePath('/uploads/images')->setUploadDir('public/uploads/images'),
            ImageField::new('image2')->setBasePath('/uploads/images')->setUploadDir('public/uploads/images'),
            ImageField::new('image3')->setBasePath('/uploads/images')->setUploadDir('public/uploads/images'),
            Field::new('imageFile')->setFormType(VichImageType::class)->hideOnForm(),
            DateTimeField::new('updatedAt')
        ];
    }
}
