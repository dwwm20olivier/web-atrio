<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Repository\ArticlesRepository;
use App\Search\Search;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, ArticlesRepository $guitare): Response
    {
        $search = new Search();
        $searchs = $guitare->findSearch($search);
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'search' => $this->renderView('includes/index.html.twig', ['searchs' => $searchs])
            ]);
        }
        return $this->render('home/index.html.twig', [

        ]);
    }

    /**
     * @Route("/search/{slug<[a-z-A-Z-0-9-\s\()-,-&-/\û\é]+>}",name="search")
     */
    public function show(Articles $item): Response
    {
        return $this->render('search/index.html.twig', ['item' => $item]);
    }
}
