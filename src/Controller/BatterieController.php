<?php

namespace App\Controller;

use App\Entity\Articles;
use App\Repository\ArticlesRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Search\Search;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BatterieController extends AbstractController
{
    /**
     * retourne une page avec toutes les batteries
     * @Route("/batterie", name="batterie")
     */
    public function index(ArticlesRepository $repository, Request $request): Response
    {
        $search = new Search();
        $items = $repository->findArticle($search, null, 2);
        $searchs = $repository->findSearch($search);
        $count = $repository->marque(null, 2);
        $result = $repository->minMax($search, 'batterie-acoustique', 2);
        dump($result);
        $min = $result[0]['min'];
        $max = $result[0]['max'];
        $categories = $repository->findCategorie(2);
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->renderView('batterie/product.html.twig', ['items' => $items]),
                'pagination' => $this->renderView('guitare/pagination.html.twig', compact('items')),
                'price' => $this->renderView('includes/price.html.twig', ['min' => $min, 'max' => $max]),
                'search' => $this->renderView('includes/index.html.twig', ['searchs' => $searchs])

            ]);
        }
        return $this->render('batterie/index.html.twig', [
            'items' => $items,
            'count' => $count,
            'min' => $min,
            'max' => $max,
            'categories' => $categories
            ]);
    }

    /**
     * retourne une page avec un article
     * @Route("/batterie/{slug<[a-z-A-Z-0-9\û]+>}",name="batterieShow")
     */
    public function show(Articles $item): Response
    {
        return $this->render('includes/show.html.twig', compact('item'));
    }

    /**
     * retourne une page avec la liste des articles filtrés par catégorie
     * @Route("/batterie-categorie/{slug}", name="showBatterieCategorie")
     */
    public function categorie($slug, Request $request, ArticlesRepository $repository): Response
    {
        $search = new Search();
        $items = $repository->findArticle($search, $slug, 2);
        $searchs = $repository->findSearch($search);
        $count = $repository->marque($slug, 2);
        $result = $repository->minMax($search, $slug, 2);
        $min = $result[0]['min'];
        $max = $result[0]['max'];
        $categories = $repository->findCategorie(2);
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'content' => $this->renderView('batterie/product.html.twig', ['items' => $items]),
                'pagination' => $this->renderView('guitare/pagination.html.twig', compact('items')),
                'price' => $this->renderView('includes/price.html.twig', ['min' => $min, 'max' => $max]),
                'search' => $this->renderView('includes/index.html.twig', ['searchs' => $searchs])

            ]);
        }
        return $this->render('batterie/index.html.twig', [
            'items' => $items,
            'count' => $count,
            'min' => $min,
            'max' => $max,
            'categories' => $categories
            ]);
    }
}
