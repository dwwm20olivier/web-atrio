<?php

namespace App\Controller;

use App\Repository\ArticlesRepository;
use App\Repository\BatterieAcoustiqueRepository;
use SessionIdInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class BasketController extends AbstractController
{
    /**
     * @Route("/panier", name="panier")
     */
    public function index(SessionInterface $session, ArticlesRepository $repository): Response
    {

        $panier = $session->get('panier', []);
        $options = [];
        foreach ($panier as $id => $quantity) {
            $options[] = [
                'product' => $repository->find($id),
                'quantite' => $quantity,
            ];
        }
        return $this->render('basket/index.html.twig', [
            'options' => $options
        ]);
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @param SessionIdInterface $session
     * @Route("/panier/add/{id<\d+>}", name="add")
     */
    public function add($id, SessionInterface $session, Request $request)
    {
        $panier = $session->get('panier', []);
        if (!empty($panier[$id])) {
            $panier[$id]++;
        } else {
            $panier[$id] = 1;
        }
        $session->set('panier', $panier);
        return $this->redirect($request->headers->get('referer'), 302);
    }

    /**
     * @Route("/panier/less/{id<\d+>}", name="less")
     *
     * @param [type] $id
     * @param SessionInterface $session
     * @param Request $request
     */
    public function moins($id, SessionInterface $session, Request $request)
    {
        $panier = $session->get('panier', []);
        if ($panier[$id] === 0) {
            $panier[$id] = 0;
        } else {
            $panier[$id]--;
        }
        $session->set('panier', $panier);
        return $this->redirect($request->headers->get('referer'), 302);
    }

    /**
     * @Route("/panier/remove/{id<\d+>}", name="remove")
     */

    public function remove($id, SessionInterface $session, Request $request)
    {
        $panier = $session->get('panier', []);
        unset($panier[$id]);
        $session->set('panier', $panier);
        dump($request->headers->get('referer'));
        return $this->redirect($request->headers->get('referer'), 302);
    }

     /**
      * @Route("/panier/info", name="total")
      */
    public function info(SessionInterface $session, ArticlesRepository $repository)
    {
        $panier = $session->get('panier', []);
        $options = [];
        foreach ($panier as $id => $quantity) {
            $options[] = [
                'product' => $repository->find($id),
                'quantite' => $quantity,
            ];
        }
        return $this->json($options, 200, [], ['groups' => 'articles']);
    }
}
