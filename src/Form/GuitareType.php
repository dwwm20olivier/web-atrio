<?php

namespace App\Form;

use App\Entity\GuitareElectrique;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GuitareType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('model')
            ->add('description')
            ->add('image')
            ->add('image2')
            ->add('image3')
            ->add('price')
            ->add('promo')
            ->add('caracteristques')
            ->add('codePromo')
            ->add('prixPromo')
            ->add('marque')
            ->add('stock')
            ->add('categorie')
            ->add('slug')
            ->add('main')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GuitareElectrique::class,
        ]);
    }
}
