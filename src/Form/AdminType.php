<?php

namespace App\Form;

use App\Entity\GuitareElectrique;
use App\Entity\Marques;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('model')
            ->add('description')
            ->add('image')
            ->add('image1')
            ->add('image2')
            ->add('price')
            ->add('promo')
            ->add('caracteristques')
            ->add('marks')
            ->add('codePromo')
            ->add('categorie')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GuitareElectrique::class,
        ]);
    }
}
