<?php

namespace App\Form;

use App\Data\Search;
use App\Entity\Marques;
use App\Repository\ArticlesRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class SearchType extends AbstractType
{
    private $repo;
    public function __construct(ArticlesRepository $repo)
    {
        $this->repo = $repo;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('marque', EntityType::class, [
                'label' => false,
                'class' => Marques::class,
                'multiple' => true,
                'expanded' => true,
                'required' => false
            ])
            ->add('minprice', IntegerType::class, [
                'required' => true,

            ])
            ->add('maxprice', IntegerType::class, [
                'required' => true
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Search::class,
        ]);
    }
}
