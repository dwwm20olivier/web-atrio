<?php

namespace App\Form;

use App\Entity\AmpliGuitare;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AmpliGuitareType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('model')
            ->add('description')
            ->add('image')
            ->add('price')
            ->add('promo')
            ->add('codepromo')
            ->add('prixpromo')
            ->add('caracteristiques')
            ->add('marque')
            ->add('categorie')
            ->add('image2')
            ->add('image3')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AmpliGuitare::class,
        ]);
    }
}
