<?php

namespace App\Form;

use App\Entity\BatterieAcoustique;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BatterieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('model')
            ->add('description')
            ->add('image')
            ->add('image2')
            ->add('image3')
            ->add('price')
            ->add('promo')
            ->add('caracteristiques')
            ->add('codepromo')
            ->add('prixpromo')
            ->add('marque')
            ->add('categorie')
            ->add('stock')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BatterieAcoustique::class,
        ]);
    }
}
