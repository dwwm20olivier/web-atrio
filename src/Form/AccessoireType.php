<?php

namespace App\Form;

use App\Entity\Accessoires;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccessoireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('model')
            ->add('description')
            ->add('image')
            ->add('price')
            ->add('promo')
            ->add('codepromo')
            ->add('prixpromo')
            ->add('caracteristiques')
            ->add('marque')
            ->add('categorie')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Accessoires::class,
        ]);
    }
}
